from youtube_dl import YoutubeDL
import youtube_dl
import requests


def downloadgg(fid, gid):
    ydl_params = {
        'format': "bestvideo+bestaudio/best",
    }
    get_link = YoutubeDL(params=ydl_params)
    gdiver_url = f'https://drive.google.com/file/d/{gid}/edit'
    data = get_link.extract_info(url=gdiver_url, download=False)
    res = dict(id=data['id'], title=data['title'], duration=data['duration'], itag=data['format_id'],
               format=data['format'])

    try:
        with youtube_dl.YoutubeDL(ydl_params) as ydl:
            ydl.download([gdiver_url])
            path_file = f"./{fid}.mp4"

            res['file'] = path_file

            url = f'https://content.googleapis.com/drive/v2/files/{data["id"]}?key=AIzaSyCzXPwAONGlcvo_dV0c6ICxql3Wbp2Hkxk'
            api_data = requests.get(url=url).json()
            if 'md5Checksum' in api_data:
                res['md5Checksum'] = api_data['md5Checksum']
                res['fileSize'] = api_data['fileSize']
            else:
                res['md5Checksum'] = ''

            return dict(status=1, data=res)
    except:
        return dict(status=0)


downloadgg(fid='abc', gid='1__p9dbKv2GMpxDxKUNzyHoNssTgkrrEg')
